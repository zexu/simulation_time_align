#include <algorithm>
#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <string>
#include <iostream>

bool isFileExists_access(string& name) {
    return (access(name.c_str(), F_OK ) != -1 );
}

std::vector<std::string> split_stri(std::string s,char token){
   std::stringstream iss(s);
   std::string word;
   std::vector<std::string> vs;
   while(getline(iss,word,token)){
        vs.push_back(word);
   }
   return vs;
}

void trim(string &s)
{
        if( !s.empty() )
        {
                s.erase(0,s.find_first_not_of(" "));
                s.erase(s.find_last_not_of(" ") + 1);
        }
}



void getValue_first(std::string target, std::vector<double> &save_vec, int h){

	ifstream myfile(Form("../out_%d.txt",h));
	if (!myfile.is_open()){ 
		cout << "can not open this file" << endl; 
	}

	string::size_type idx;
	string s;
	while(getline(myfile, s)){

		idx=s.find(target);
		if(idx != string::npos){

			string str_num = s.erase(0,target.length());
			//cout << target << endl;
			//cout << str_num << endl;
			trim( str_num );
			//cout << str_num << endl;
			std::vector<std::string> splitname = split_stri(str_num, ' ' );
			for( auto value : splitname ){
				cout << "h : " << h << " value : " << value << "  " << endl;
				auto local_idx=value.find("e");
				if( local_idx != string::npos ) { save_vec.push_back( 0.0 ); }
				else{ save_vec.push_back( std::stof( value ) ); }
				///save_vec.push_back( std::stof( value ) );	
			}
			if( save_vec.size()>=2 ){ break; }

		}// not end
	}// line loop

}
void draw(){

	gROOT->ProcessLine(".x ~/lhcbStyle.C");
	gStyle->SetOptStat(0);

	ofstream outfile;
	outfile.open("shift.txt");

	int maxmodule=5;
	for(int l=0; l<maxmodule; l++){ // module
		for(int s=0; s<2; s++){
			std::string target_string = Form("T%dL%dQ%dM%dH%d ", 1, 0, 0, l,s);
			std::cout << target_string << std::endl;

			std::vector<double> vec_t1;
			std::vector<double> vec_t2;

			int Y_range = 5;
			if( l==3 && s==0 ) { Y_range=4; }
			if( l==4 && s==1 ) { Y_range=3; }
			if( l==4 && s==0 ) { Y_range=2; }

			
			for(int h=0; h<Y_range; h++){

				std::vector<double> first_raw_vec;
				first_raw_vec.clear();
				getValue_first( target_string, first_raw_vec, h );
				//std::cout << "vector size fisrt : " << first_raw_vec.size() << std::endl;

				vec_t1.push_back( first_raw_vec[1] );
				vec_t2.push_back( first_raw_vec[2] );
			}

#if 0
			for(vector<double>::iterator it = vec_t1.begin(); it != vec_t1.end(); it++){
				std::cout << " test t1 : " << *it << std::endl;
				if( *it<5 or *it>50 ) { vec_t1.erase(it);  }
				std::cout << " size : " << vec_t1.size() << std::endl;
			}
#endif

			for(vector<double>::iterator it = vec_t2.begin(); it != vec_t2.end(); it++){
				std::cout << " test t2 : " << *it << std::endl;
				if( *it<5 or *it>50 ) { vec_t2.erase(it);  }
			}
				
	
		 	double max_t1 = *max_element(vec_t1.begin(), vec_t1.end());
		 	double min_t2 = *min_element(vec_t2.begin(), vec_t2.end());

			outfile << target_string 
				//<< " " << (max_t1+min_t2)/2.
				<< " " << max_t1 + 0.35*(min_t2-max_t1)
				<< " " << (min_t2-max_t1)
				<< " " << max_t1
				<< " " << min_t2
				<< "\n";


		}
	}



}
